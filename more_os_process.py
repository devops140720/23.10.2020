import os
print(os.environ["PATH"]) # get specific variable -- pth
print(os.getenv('PATH')) # get path in a different way
print(os.getenv('os')) # get os variable using getenv
print('========= all variables in os ============')
d = os.environ.__dict__
d1 = d["_data"]
for k in d1:
    print(f'{k} : {d1[k]}')
print('=====================')
path_env = os.environ["PATH"]
# DANGER : os.environ["PATH"] = path_env + ";d:\itay";

file_name = r'd:\itay\hello.txt'
print(f'os.path.isfile({file_name})? ', os.path.isfile(file_name))
print(f'os.path.isfile({"stam"})? ', os.path.isfile("stam"))
print(f'os.path.isfile(d:\itay)? ', os.path.isfile(r'd:\itay'))
print(f'os.path.isdir(d:\itay)? ', os.path.isdir(r'd:\itay'))
print(f'os.path.isdir({file_name})? ', os.path.isdir(file_name))
print(f'os.path.exists({file_name})',os.path.exists(file_name))
print(f'os.path.exists({"aaa" + file_name})',os.path.exists('aaa' + file_name))
print(f'os.path.exists({str("d:/itay")})',os.path.exists(r'd:\itay'))
print(f'os.path.exists({str("d:/itay12")})',os.path.exists(r'd:\itay12'))

#os.startfile(r'c:\windows\System32\notepad.exe')
print('done.........')
#os.startfile(r'D:\itay\pdfs.zip')

import stat

filAttr = os.stat(file_name)
os.chmod(file_name, stat.S_IWRITE)
#os.chmod(file_name, stat.S_IREAD)
#os.startfile(file_name)
#os.chmod(file_name, stat.S_IWRITE)

import psutil
danger = []
for proc in psutil.process_iter():
    try:
        print(proc.name())
        if proc.name() == "notepad.exe":
            proc.kill()
    except:
        danger.append(proc.name)
        print('danger')
print(danger)