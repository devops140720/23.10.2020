import utils1.filesfoldersapi as fapi

def targil1():
    fname = input("Which file are you looking for? ")
    dname = input("From which directory to start? ")
    fapi.find_file(fname, dname)


def targil2():
    fname = input("Please type the extension (i.e. *.txt) ")
    dname = input("From which directory to start? ")
    fapi.find_all_files_with_extension(fname, dname)


def targil3():
    dname = input("From which directory to start? ")
    file_name, max_size = fapi.find_biggest_file_size(dname)
    print(f'{file_name} {convert_bytes(max_size)}')

def main():
    targil1()
    targil2()
    targil3()

#print("runner:",__name__)
if __name__ == '__main__':
    main()